package cz.cvut.dsv.DTO;

public enum MessageStatus {
    SUCCESSFUL, NOT_ENOUGH_FUNDS, NO_RECIPIENT, ILLEGEL_RECIPIENT
}
