package cz.cvut.dsv.DTO;

import java.io.Serializable;
import java.util.HashMap;

public class InitData implements Serializable {
    private SingleNode nextNode;
    private SingleNode prevNode;
    private HashMap<String, Integer> table;
    private int tableVersion;
    private long logicalTime;

    public InitData(SingleNode nextNode, SingleNode prevNode, HashMap<String, Integer> table, int tableVersion, long logicalTime) {
        this.nextNode = nextNode;
        this.prevNode = prevNode;
        this.table = table;
        this.tableVersion = tableVersion;
        this.logicalTime = logicalTime;
    }

    public SingleNode getNextNode() {
        return nextNode;
    }

    public void setNextNode(SingleNode nextNode) {
        this.nextNode = nextNode;
    }

    public SingleNode getPrevNode() {
        return prevNode;
    }

    public void setPrevNode(SingleNode prevNode) {
        this.prevNode = prevNode;
    }

    public HashMap<String, Integer> getTable() {
        return table;
    }

    public void setTable(HashMap<String, Integer> table) {
        this.table = table;
    }

    public int getTableVersion() {
        return tableVersion;
    }

    public void setTableVersion(int tableVersion) {
        this.tableVersion = tableVersion;
    }

    public long getLogicalTime() {
        return logicalTime;
    }
}
