package cz.cvut.dsv.DTO;

import java.io.Serializable;

/**
 * Message send to sender and to recipient if payment successful
 *
 *
 */
public class MessageBalanceUpdate implements Serializable {
    private String otherNode;

    private Integer prevAmount;
    private Integer currentAmount;

    private MessageStatus status;
    private long logicalTime;

    public MessageBalanceUpdate(String otherNode, Integer prevAmount, Integer currentAmount, MessageStatus status, long logicalTime) {
        this.otherNode = otherNode;
        this.prevAmount = prevAmount;
        this.currentAmount = currentAmount;
        this.status = status;
        this.logicalTime = logicalTime;
    }

    public String getOtherNode() {
        return otherNode;
    }

    public Integer getPrevAmount() {
        return prevAmount;
    }

    public Integer getCurrentAmount() {
        return currentAmount;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public long getLogicalTime() {
        return logicalTime;
    }

    @Override
    public String toString() {
        return "MessageBalanceUpdate [status=" + status + "]";
    }
}
