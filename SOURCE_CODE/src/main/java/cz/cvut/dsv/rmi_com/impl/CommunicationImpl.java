package cz.cvut.dsv.rmi_com.impl;

import cz.cvut.dsv.DTO.*;
import cz.cvut.dsv.rmi_com.Communication;
import cz.cvut.dsv.rmi_com.NetNode;
import cz.cvut.dsv.rmi_com.PaymentOperation;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.dsv.DTO.MessageType.*;

public class CommunicationImpl extends UnicastRemoteObject implements Communication, PaymentOperation {
    private final Integer initFunds = 100;

    private boolean voting;
    private SingleNode leader = null;

    private Set<Node> participants = Collections.synchronizedSet(new HashSet<Node>());
    private boolean repairing;

    private Logger LOG = Logger.getLogger(CommunicationImpl.class.getName());

    public CommunicationImpl() throws RemoteException {
        LOG.setLevel(Level.INFO);
//        try {
//            LOG.addHandler(new FileHandler("/home/dsv/Documents/logger02.log", 1024 * 1024 * 1024, 1, false));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        super();
    }

    public InitData appendNewNode(SingleNode newNode, long logicalTime) throws RemoteException {
        compareAndSetLogicalTime(logicalTime);

        if(leader != null){
            if(!isLeader()) {
                try {
                    LOG.log(Level.FINE, "LT-" + getLogicalTime() + " " + "#appendNewNode: forwarding to leader = " + leader.getAddress());
                    Communication leaderComm = NetNode.getInstance().getRemoteService(leader);
                    return leaderComm.appendNewNode(newNode, incrementLogicalTime());
                } catch (Exception e) {
                    repairRing();
                    LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#appendNewNode: " + e);
                }
            }
        }

        final NetNode netNode = NetNode.getInstance();
        final Node node = netNode.getNode();
        SingleNode nextNode;
        SingleNode prevNode;
        synchronized (node) {
            if (node.getNextNode() == null || node.getPrevNode() == null){
                //SINGLE NODE IN NETWORK INIT TABLE AND ITS ACCOUNT
                LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#appendNewNode: starting new Network i'm the only one");
                node.setNextNode(newNode);
                node.setPrevNode(newNode);
                nextNode = newNode;
                prevNode = newNode;

                HashMap<String, Integer> table = NetNode.getInstance().getBalanceTable();
                table.put(newNode.getAddress(), initFunds);

//                sendTableUpdateMessage();

                return new InitData(nextNode, prevNode, table, netNode.getTableVersion(), incrementLogicalTime());
            } else {
                //MORE THAN ONE NODE IN NETWORK ADDING ACCOUNT FOR NEW NODE
                //NEW NODE ADDED BETWEEN LEADER AND NEXT TO LEADER
                nextNode = node.getNextNode();
                prevNode = node.getMe();
                node.setNextNode(newNode);

                HashMap<String, Integer> table = NetNode.getInstance().getBalanceTable();

                if(!table.containsKey(newNode.getAddress())) {
                    table.put(newNode.getAddress(), initFunds);
                    netNode.setTableVersion(netNode.getTableVersion() + 1);
                    LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#appendNewNode: creating account and appending new node = " + newNode.getAddress());
                } else {
                    LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#appendNewNode: node already has account, appending new node = " + newNode.getAddress());
                }

                try{
                    final Communication communication = netNode.getRemoteService(nextNode);
                    communication.setPrev(newNode);
                } catch(Exception e){
                    repairRing();
                    throw new RemoteException("LT-" + getLogicalTime() + " " + "#appendNewNode - failed");
                }

                return new InitData(nextNode, prevNode, table, netNode.getTableVersion(), incrementLogicalTime());
            }
        }
    }

    public void onMessage(Message msg) throws RemoteException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#onMessage: message = " + msg);
        final SingleNode me = getNetNode().getNode().getMe();

        // null = start election
        if (msg==null) {
            sendToNext(new Message(ELECTION, me, incrementLogicalTime()));
            return;
        }

        compareAndSetLogicalTime(msg.getLogicalTime());
        msg.setLogicalTime(getLogicalTime());

        switch (msg.getType()) {
            case ELECTION:
                setRepairing(false);
                if (!voting) {
                    // erasing collection if first round of election
                    synchronized (participants) {
                        participants.clear();
                    }
                }
                voting=true;
                if (msg.getNode().getId()==me.getId()) {
                    msg.setType(ELECTED);
                } else if (msg.getNode().getId()<me.getId()) {
                    msg.setNode(me);
                }
                sendToNext(msg);
                break;
            case ELECTED:
                voting=false;
                leader=msg.getNode();
                LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#onMessage: new leader is = " + leader.getAddress());
                final Communication communication;
                try {
                    communication = getNetNode().getRemoteService(leader);
                } catch (Exception e) {
                    repairRing();
                    LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#onMessage:" + e);
                    throw new RemoteException(e.getLocalizedMessage());
                }
                communication.register(getNetNode().getNode());
                if (leader.getId()!=me.getId()){
                    sendToNext(msg);
                } else {
                    //UPDATING TABLE TO CURRENT LEADER
                    sendTableUpdateMessage();
                }
                setRepairing(false);
                break;
            case LOOK_NEXT:
                if(!isRepairing()) {
                    setRepairing(true);
                    try {
                        sendToNext(msg, false);
                    } catch (Exception e) {
                        LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#onMessage: " + e);
                        msg.setNode(me);
                        msg.setType(LOOK_PREV);
                        try {
                            sendToPrev(msg, false);
                        } catch (Exception e2) {
                            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#onMessage: " + e2);
                            // ONLY ONE NODE LEFT IN NETWORK
                            final Node node = getNetNode().getNode();
                            synchronized (node) {
                                node.setPrevNode(msg.getNode());
                                node.setNextNode(msg.getNode());
                            }
                            onMessage(null);
                        }
                    }
                }
                break;
            case LOOK_PREV:
                try {
                    sendToPrev(msg, false);
                } catch (Exception e) {
                    try {
                        final Node node = getNetNode().getNode();
                        synchronized (node) {
                            node.setPrevNode(msg.getNode());
                        }
                        final Communication communicationPrev = getNetNode().getRemoteService(msg.getNode());
                        communicationPrev.setNext(me);
                        onMessage(null);
                    } catch (Exception e2) {
                        LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#onMessage: " + e2);
                        repairRing();
                    }
                }
                break;
        }
    }

    private void sendToNext(Message msg) throws RemoteException {
        final Node node=getNetNode().getNode();
        synchronized (node) {
            sendTo(node.getNextNode(), msg, true);
        }
    }

    private void sendToNext(Message msg, boolean attemptToFixOnError) throws RemoteException {
        final Node node=getNetNode().getNode();
        synchronized (node) {
            sendTo(node.getNextNode(), msg, attemptToFixOnError);
        }
    }

    private void sendToPrev(Message msg, boolean attemptToFixOnError) throws RemoteException {
        final Node node=getNetNode().getNode();
        synchronized (node) {
            sendTo(node.getPrevNode(), msg, attemptToFixOnError);
        }
    }

    private void sendTo(SingleNode destinationNode,final Message msg, boolean attemptToFixOnError) throws RemoteException {
        try {
            final Communication communication=getNetNode().getRemoteService(destinationNode);
            new Thread(new Runnable(){
                @Override
                public void run() {
                    try {
                        communication.onMessage(msg);
                    } catch (RemoteException e) {
                        LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#sendTo:" + e);
                        try {
                            repairRing();
                        } catch (RemoteException e1) {
                            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#sendTo:" + e1);
                        }
                    }
                }}).start();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#sendTo:" + e);
            if (attemptToFixOnError) {
                repairRing();
            }
            throw new RemoteException(e.getLocalizedMessage());
        }
    }

    public void setNext(SingleNode newNode) throws RemoteException {
        final NetNode netNode;
        try{
            netNode = NetNode.getInstance();
        } catch(Exception e){
            throw new RemoteException("LT-" + getLogicalTime() + " " + "#setNext");
        }

        final Node node = netNode.getNode();
        synchronized (node){
            node.setNextNode(newNode);
        }
    }

    public void setPrev(SingleNode newNode) throws RemoteException {
        final NetNode netNode;
        try{
            netNode = NetNode.getInstance();
        } catch(Exception e){
            throw new RemoteException("LT-" + getLogicalTime() + " " + "#setPrev");
        }

        final Node node = netNode.getNode();
        synchronized (node){
            node.setPrevNode(newNode);
        }
    }

    public void register(Node me) throws RemoteException {
        synchronized (participants) {
            participants.add(me);
        }
    }

    public void disconnect(Node me, boolean voteNewLeader) throws RemoteException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#disconnect: quiting this network as = " + me.getMe().getAddress());
        try {
//            if (isLeader()) {
                synchronized (participants) {
                    participants.remove(me);
                }
                final Communication communicationNext=getNetNode().getRemoteService(me.getNextNode());
                final Communication communicationPrev=getNetNode().getRemoteService(me.getPrevNode());
                communicationNext.setPrev(me.getPrevNode());
                communicationPrev.setNext(me.getNextNode());
                // NEW LEADER ELECTION starting
//                if (voteNewLeader == true){
                    sendTo(me.getNextNode(), null, false);
//                }
//            } else {
//                final Communication communication=getNetNode().getRemoteService(leader);
//                communication.disconnect(me, false);
//            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#disconnect:" + e);
            repairRing();
        }
    }

    public void repairRing() throws RemoteException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#repairRing: repairing of the ring was started");
        final Message msg = new Message(LOOK_NEXT, getNetNode().getNode().getMe(), incrementLogicalTime());
        onMessage(msg);
    }

    public boolean isRepairing() throws RemoteException {
        return repairing;
    }

    public SingleNode getLeader() throws RemoteException {
        try {
            getNetNode().getRemoteService(leader);
        } catch (Exception e) {
            repairRing();
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#getLeader: " + e);
        }
        return leader;
    }

    public void setRepairing(boolean repairing) throws RemoteException {
        this.repairing = repairing;
    }

    public void transfer(Payment pmt) throws RemoteException {
        if(isLeader()){

            compareAndSetLogicalTime(pmt.getLogicalTime());

            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#transfer: " + pmt);
            NetNode leader = NetNode.getInstance();

            HashMap<String, Integer> table = leader.getBalanceTable();

            String sender = pmt.getSender().getAddress();
            String recipient = pmt.getRecipient();
            Integer amount = pmt.getAmount();

            if(pmt.getRecipient().equals(pmt.getSender().getAddress())){
                MessageBalanceUpdate msg = new MessageBalanceUpdate(pmt.getSender().getAddress(), pmt.getAmount(), pmt.getAmount(), MessageStatus.ILLEGEL_RECIPIENT, incrementLogicalTime());
                sendBalanceUpdateMessage(msg, sender);
                return;
            }

            synchronized (table) {
                if (table.containsKey(sender)) {
                    Integer senderBalance = table.get(sender);
                    //DOES HAVE SENDER ENOUGH FUNDS?
                    if (senderBalance >= amount) {
                        //DOES RECIPIENT EXIST?
                        if (table.containsKey(recipient)) {
                            Integer recipientBalance = table.get(recipient);
                            table.put(sender, senderBalance - amount);
                            table.put(recipient, recipientBalance + amount);
                            leader.setBalanceTable(table);
                            leader.setTableVersion(leader.getTableVersion() + 1);
                            leader.setTableVersion(leader.getTableVersion());

                                MessageBalanceUpdate msgToSender = new MessageBalanceUpdate(recipient, senderBalance, senderBalance - amount, MessageStatus.SUCCESSFUL, incrementLogicalTime());
                                MessageBalanceUpdate msgToRecipient = new MessageBalanceUpdate(sender, recipientBalance, recipientBalance + amount, MessageStatus.SUCCESSFUL, incrementLogicalTime());

                            //INFORMING sender and recipient about SUCCESSFUL transaction
                            try {
                            sendBalanceUpdateMessage(msgToRecipient, recipient);
                            sendBalanceUpdateMessage(msgToSender, sender);
                            } catch (Exception e){
                                LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#transfer: " + e);
                                repairRing();
                            }

                            sendTableUpdateMessage();

                        } else {
                            //RECIPIENT DOESNT EXIST
                            MessageBalanceUpdate msg = new MessageBalanceUpdate(recipient, pmt.getAmount(), pmt.getAmount(), MessageStatus.NO_RECIPIENT, incrementLogicalTime());
                            sendBalanceUpdateMessage(msg, sender);
                        }
                    } else {
                        //NOT ENOUGH CURRENCY IN ACCOUNT
                        MessageBalanceUpdate msg = new MessageBalanceUpdate(sender, pmt.getAmount(), pmt.getAmount(), MessageStatus.NOT_ENOUGH_FUNDS, incrementLogicalTime());
                        sendBalanceUpdateMessage(msg, sender);
                    }
                }
            }
        } else {
            repairRing();
            throw new RemoteException("LT-" + getLogicalTime() + " " + "#transfer - insufficient permissions");
        }
    }

    public Integer getAccountBalance(SingleNode node, long logicalTime) throws RemoteException {
        compareAndSetLogicalTime(logicalTime);

        if(isLeader()){
            try {
                return getNetNode().getAccountBalance(node);
            } catch (NotBoundException | MalformedURLException e) {
                LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#getAccountBalance: " + e);
            }
        } else {
            repairRing();
            throw new RemoteException("LT-" + getLogicalTime() + " " + "#getAccountBalance - insufficient permissions");
        }
        return null;
    }

    public void accountBalanceUpdate(MessageBalanceUpdate msg) throws RemoteException {
        compareAndSetLogicalTime(msg.getLogicalTime());

        MessageStatus status = msg.getStatus();
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#accountBalanceUpdate: " + msg);
        switch(status) {
            case ILLEGEL_RECIPIENT: {
                System.out.println("YOU CANNOT SEND CURRENCY TO YOURSELF");
                break;
            }
            case NOT_ENOUGH_FUNDS: {
                System.out.println("NOT ENOUGH CURRENCY IN ACCOUNT");
                break;
            }
            case NO_RECIPIENT: {
                System.out.println("RECIPIENT DOESNT EXIST");
                break;
            }
            case SUCCESSFUL: {
                if (msg.getPrevAmount() < msg.getCurrentAmount()) {
                    System.out.println(msg.getOtherNode() + " has send you: " + (msg.getCurrentAmount() - msg.getPrevAmount()) + "$");
                } else {
                    System.out.println("You have send " + (msg.getPrevAmount() - msg.getCurrentAmount()) + "$ to " + msg.getOtherNode());
                }
                break;
            }
        }
    }

    public void updateBalanceTable(MessageTableUpdate msg) throws RemoteException {
        if(!isLeader()) {

            compareAndSetLogicalTime(msg.getLogicalTime());

            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#updateBalanceTable: updating balance table version = " + msg.getTableVersion());
            NetNode me = NetNode.getInstance();

            synchronized (me) {
                if (me.getTableVersion() < msg.getTableVersion()) {
                    me.setTableVersion(msg.getTableVersion());
                    me.setBalanceTable(msg.getBalanceTable());
                }
            }

            //SENDING UPDATE TO NEXT NODE
            PaymentOperation pmtOps = null;
            try {
                pmtOps = NetNode.getInstance().getRemoteService(me.getNode().getNextNode());
            } catch (Exception e) {
                LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#updateBalanceTable: " + e);
                repairRing();
            }

            msg.setLogicalTime(getLogicalTime());

            pmtOps.updateBalanceTable(msg);

        } else {
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#updateBalanceTable: balance table has been updated on all nodes");
        }
    }

    private NetNode getNetNode(){
        return NetNode.getInstance();
    }

    private boolean isLeader() throws RemoteException {
        final SingleNode node = getNetNode().getNode().getMe();
        synchronized (node) {
            boolean result = node.equals(leader);
            return result;
        }
    }

    private void sendTableUpdateMessage() throws RemoteException {
        if(isLeader()) {
            NetNode leader = NetNode.getInstance();
            PaymentOperation pmtOps = null;
            try {
                pmtOps = leader.getRemoteService(leader.getNode().getNextNode());
            } catch (NotBoundException | MalformedURLException e) {
                repairRing();
                LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#sendBalanceTableMessage: " + e);
            }

            MessageTableUpdate msg = new MessageTableUpdate(leader.getBalanceTable(), leader.getTableVersion(), incrementLogicalTime());

            pmtOps.updateBalanceTable(msg);
        }
    }

    private void sendBalanceUpdateMessage(MessageBalanceUpdate msg, String recipient) throws RemoteException {
        PaymentOperation recipientPmtOps = null;
        try {
            recipientPmtOps = getNetNode().getRemoteService(new SingleNode(recipient));
            recipientPmtOps.accountBalanceUpdate(msg);
        } catch (NotBoundException | MalformedURLException e) {
            repairRing();
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#sendBalanceUpdateMessage: " + e);
        }
    }

    private long getLogicalTime() {
        return getNetNode().getLogicalTime();
    }

    private void compareAndSetLogicalTime(long logicalTime) {
        getNetNode().compareAndSetLogicalTime(logicalTime);
    }

    private long incrementLogicalTime(){
        return getNetNode().incrementLogicalTime();
    }

}
