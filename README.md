# DSV-LE-ChangRoberts-Payments-Java_RMI

Semestrální práce na předmět DSV-2019
Erben Jaroslav 

|||
| ------ | ------ |
| PDF s popisem systému | /DOCS |
| Zdrojový kód | /SOURCE_CODE/ |

Zadání
* Platební systém
* Volba vůdce
* CHang-Roberts
* Java RMI

Vůdce bude sloužit jako synchronizátor a zaručuje, že žádný uzel neposílá více měny než má na účtu a nedochází k double spendingu. Uzly mohou pouze převádět měnu ze svého účtu na jiný. Žádost o převod hlásí vůdci, který žádost vyhodnotí a případně vykoná, následně o změně stavu účtů dá vědět všem uzlům, opačně informuje žadatele, že nelze vyhovět. Stavy všech účtů si budou držet všechny uzly v síti. Daný účet bude svázán s unikátním identifikátorem, nejspíše IP:PORT jeho majitele při vytvoření. Při připojení k síti, pokud není veden účet na jeho identifikátor, je uzlu vytvořen nový účet s daným počtem měny. V případě úmrtí vůdce je možné, že dojde ke ztrátě některých dat.

