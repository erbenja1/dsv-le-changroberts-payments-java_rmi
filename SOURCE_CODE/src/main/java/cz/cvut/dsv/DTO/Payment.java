package cz.cvut.dsv.DTO;

import java.io.Serializable;

public class Payment implements Serializable {
    private final String recipient;
    private final SingleNode sender;
    private final Integer amount;
    private final long logicalTime;

    public Payment(String recipient, SingleNode sender, Integer amount, long logicalTime) {
        this.recipient = recipient;
        this.sender = sender;
        this.amount = amount;
        this.logicalTime = logicalTime;
    }

    public String getRecipient() {
        return recipient;
    }

    public SingleNode getSender() {
        return sender;
    }

    public Integer getAmount() {
        return amount;
    }

    public long getLogicalTime(){ return logicalTime; }
}
