package cz.cvut.dsv.DTO;

import java.io.Serializable;
import java.util.Objects;

public class SingleNode implements Serializable {
    private int id;
    private String address;
    private String ip;
    private int port;

    public SingleNode(String address){
        this.address = address;
        this.ip = address.split("\\:")[0];
        this.port = Integer.valueOf(address.split("\\:")[1]);
        this.id = Integer.valueOf(ip.split("\\.")[3]);
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        SingleNode other = (SingleNode) o;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        if (ip == null) {
            if (other.ip != null)
                return false;
        } else if (!ip.equals(other.ip))
            return false;
        if (id != other.id)
            return false;
        if (port != other.port)
            return false;
        return true;
    }


    @Override
    public int hashCode() {
        return Objects.hash(id, address, ip, port);
    }
}
