package cz.cvut.dsv.DTO;

import java.io.Serializable;

public class Message implements Serializable {
    private MessageType type;
    private SingleNode node;
    private long logicalTime;

    public Message(MessageType type, SingleNode node, long logicalTime) {
        this.type = type;
        this.node = node;
        this.logicalTime = logicalTime;
    }


    public MessageType getType() {
        return type;
    }

    public SingleNode getNode() {
        return node;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public void setNode(SingleNode node) {
        this.node = node;
    }

    public long getLogicalTime() {
        return logicalTime;
    }

    public void setLogicalTime(long logicalTime) {
        this.logicalTime = logicalTime;
    }

    @Override
    public String toString() {
        return "Message [type=" + type + ", node=" + node + "]";
    }
}
