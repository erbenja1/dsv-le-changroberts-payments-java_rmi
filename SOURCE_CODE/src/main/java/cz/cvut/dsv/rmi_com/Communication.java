package cz.cvut.dsv.rmi_com;

import cz.cvut.dsv.DTO.InitData;
import cz.cvut.dsv.DTO.Message;
import cz.cvut.dsv.DTO.Node;
import cz.cvut.dsv.DTO.SingleNode;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Communication extends Remote {

    public InitData appendNewNode(SingleNode newNode, long logicalTime) throws RemoteException;

    public void onMessage(Message msg) throws RemoteException;

    public void setNext(SingleNode newNode) throws RemoteException;
    public void setPrev(SingleNode newNode) throws RemoteException;

    public void register(Node me) throws RemoteException;
    public void disconnect(Node me, boolean voteNewLeader) throws RemoteException;

    public void repairRing() throws RemoteException;

    public boolean isRepairing() throws RemoteException;
    public SingleNode getLeader() throws RemoteException;
    public void setRepairing(boolean repairing) throws RemoteException;
}
