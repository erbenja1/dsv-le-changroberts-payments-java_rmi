package cz.cvut.dsv.DTO;

public enum MessageType {
    ELECTION,
    ELECTED,
    LOOK_PREV,
    LOOK_NEXT,
}
