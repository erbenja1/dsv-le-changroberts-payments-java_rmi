package cz.cvut.dsv.rmi_com;

import cz.cvut.dsv.DTO.InitData;
import cz.cvut.dsv.DTO.Node;
import cz.cvut.dsv.DTO.Payment;
import cz.cvut.dsv.DTO.SingleNode;
import cz.cvut.dsv.rmi_com.impl.CommunicationImpl;

import java.net.MalformedURLException;
import java.net.NetPermission;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NetNode {
    private static NetNode INSTANCE;

    private HashMap<String, Integer> balanceTable = new HashMap<>();
    private Integer tableVersion = 0;

    private Node node = new Node();
    private Communication communication;

    private long logicalTime = 0;

    private Logger LOG = Logger.getLogger(NetPermission.class.getName());

    private NetNode(){
        super();
        LOG.setLevel(Level.INFO);
//        try {
//            LOG.addHandler(new FileHandler("/home/dsv/Documents/logger01.log", 1024 * 1024 * 1024, 1, false));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public static NetNode getInstance(){
        if(INSTANCE == null){
            INSTANCE = new NetNode();
        }
        return INSTANCE;
    }

    public void init(String address){
        this.node = new Node(address);
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Node getNode(){
        return node;
    }

    public <T> T getRemoteService(SingleNode node) throws RemoteException, NotBoundException, MalformedURLException {
        if (this.node.getMe().equals(node)) {
            return (T) communication;
        }
        return (T) Naming.lookup("//" + node.getAddress() + "/payments");
    }

    public void bind() throws RemoteException, AlreadyBoundException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#bind: listening on port = " + String.valueOf(this.node.getMe().getPort()));
        final Registry registry = LocateRegistry.createRegistry(node.getMe().getPort());
        registry.bind("payments", communication = new CommunicationImpl());
    }
    public void unbind() throws RemoteException, NotBoundException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#unbind: stopping listening on port = " + String.valueOf(this.node.getMe().getPort()));
        final Registry registry = LocateRegistry.createRegistry(node.getMe().getPort());
        registry.unbind("payments");
    }

    public void connectTo(SingleNode node) throws RemoteException, NotBoundException, MalformedURLException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#connectTo: connecting to " + String.valueOf(node.getAddress()));
        final Communication communication = getRemoteService(node);
        final InitData initData = communication.appendNewNode(this.node.getMe(), incrementLogicalTime());

        synchronized (this.node) {
            this.node.setNextNode(initData.getNextNode());
            this.node.setPrevNode(initData.getPrevNode());
            this.setBalanceTable(initData.getTable());
            this.setTableVersion(initData.getTableVersion());
            compareAndSetLogicalTime(initData.getLogicalTime());
        }

        communication.onMessage(null);
    }

    public PaymentOperation getPaymentLink() throws RemoteException, NotBoundException, MalformedURLException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#getPaymentLink");
        final SingleNode leader = communication.getLeader();
        return getRemoteService(leader);
    }

    public void sendMoney(String address, Integer amount) {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#sendMoney: sending " + amount + "$ to = " + address);
        PaymentOperation paymentOperation = null;
        Payment payment = null;
        try {
            paymentOperation = getPaymentLink();
            payment = new Payment(address, getNode().getMe(), amount, incrementLogicalTime());
            paymentOperation.transfer(payment);
        } catch (Exception e){
            System.out.println("PROBLEM with connection try gain later");
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#send: error with sending -> " + e);
            repairRing();
            System.out.println("AFTER REPAIR RING");
            sendMoney(address, amount);
        }
    }

    public HashMap<String, Integer> getBalanceTable(){
        return balanceTable;
    }

    public void setBalanceTable(HashMap<String, Integer> balanceTable) {
        this.balanceTable = balanceTable;
    }

    public void setTableVersion(Integer tableVersion) {
        this.tableVersion = tableVersion;
    }

    public Integer getAccountBalance(SingleNode node) throws NotBoundException, MalformedURLException {
        LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#getAccountBalance: asking for account of = " + node.getAddress());
        try {
            SingleNode leader = communication.getLeader();
            if(this.node.getMe().equals(leader)){
                return balanceTable.get(node.getAddress());
            }
            PaymentOperation paymentOperation = getPaymentLink();

            return paymentOperation.getAccountBalance(node, incrementLogicalTime());
        } catch (Exception e){
            repairRing();
            return getAccountBalance(node);
        }
    }

    public int getTableVersion(){
        return tableVersion;
    }

    public String getLeaderAddress() throws RemoteException {
        SingleNode leader = communication.getLeader();
        return leader.getAddress();
    }

    private void repairRing(){
        try {
            LOG.log(Level.INFO, "LT-" + getLogicalTime() + " " + "#repairRing: repairing ring from netNode = " + this.node.getMe().getAddress());
            communication.setRepairing(true);
            communication.repairRing();
            // WAITING WHILE RING REPAIRING AND ELECTIONS
            while(communication.isRepairing()){
                Thread.sleep(200);
            }
        } catch (RemoteException | InterruptedException e) {
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#repairRing: " + e);
        }
    }

    public void onClose() {
        try {
            communication.disconnect(this.getNode(), true);
        } catch (RemoteException e) {
            LOG.log(Level.SEVERE, "LT-" + getLogicalTime() + " " + "#onCLose:" + e);
        }
    }

    public long getLogicalTime() {
        return logicalTime;
    }

    public void compareAndSetLogicalTime(long logicalTime) {
        if(this.logicalTime < logicalTime) {
            this.logicalTime = logicalTime;
        }
    }

    public long incrementLogicalTime(){
        this.logicalTime += 1;
        return getLogicalTime();
    }
}
