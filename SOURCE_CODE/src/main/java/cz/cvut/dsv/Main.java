package cz.cvut.dsv;

import cz.cvut.dsv.DTO.SingleNode;
import cz.cvut.dsv.rmi_com.NetNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {
    
    public static void main(final String[] args) throws UnknownHostException {
//        LOG.info("#main: starting");
        if (args.length>3) {
            System.out.println("Error: too many arguments\n");
            System.out.println("Usage examples:");
            System.out.println(" 127.0.1.1:2010");
            System.out.println(" 127.0.1.1:2011 127.0.1.1:2010");
            return;
        }

        String myAddress="127.0.1.1:2010";
        String remoteAddress=null;
//        String name="Unknown";

        if (args.length>=1) {
            myAddress=args[0];
            System.setProperty("java.rmi.server.hostname",myAddress.split("\\:")[0]);
        }
        if (args.length>=2) {
            remoteAddress=args[1];
        }


//        if (args.length==3) {
//            name=args[2];
//        }

        final NetNode netNode;
        try {

            netNode = NetNode.getInstance();
//            LOG.debug("#main: my address = " + myAddress);
            netNode.getNode().setMe(new SingleNode(myAddress));
            netNode.bind();
            if (remoteAddress == null) {
                // připojuji se sám k sobě (jsem první uzel)
                netNode.connectTo(new SingleNode(myAddress));
            } else {
                netNode.connectTo(new SingleNode(remoteAddress));
                System.out.println("CONNECTED TO " + remoteAddress);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
            return;
        }

        /*
         * Nefunguje jak má, alespoň v IDE (neozkoušeno v konzoli)
         */
		/*Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
	        public void run() {
	        	netNode.onClose();
	        } 
		}));
		*/

//        LOG.debug("#info: Initialization was complete");
        final BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line;
            String[] lineSplitted;
            try {
                line = br.readLine();
                lineSplitted = line.split(" ");

                switch(lineSplitted[0]){
                    case "send":{
                        if(lineSplitted.length == 3) {
                            netNode.sendMoney(lineSplitted[1], Integer.valueOf(lineSplitted[2]));
                        } else {
                            System.out.println("WRONG NUMBER OF PARAMETERS - type help for command list");
                        }
                        break;
                    }
                    case "balance":{
                        System.out.println("Your balance is: " + netNode.getAccountBalance(netNode.getNode().getMe()) + " $");
                        break;
                    }
                    case "leader":{
                        System.out.println(netNode.getLeaderAddress());
                        break;
                    }
                    case "table":{
                        HashMap<String, Integer> table = netNode.getBalanceTable();
                        Iterator it = table.entrySet().iterator();
                        System.out.println("---TABLE OF RECORDS---");
                        while(it.hasNext()){
                            Map.Entry pair = (Map.Entry) it.next();
                            System.out.println(pair.getKey() + ": " + pair.getValue() + "$");
                        }
                        System.out.println("--------------------");
                        break;
                    }
                    case "next":{
                        System.out.println(netNode.getNode().getNextNode().getAddress());
                        break;
                    }
                    case "prev":{
                        System.out.println(netNode.getNode().getPrevNode().getAddress());
                        break;
                    }
                    case "tversion":{
                        System.out.println("Your table is version #" + netNode.getTableVersion());
                        break;
                    }
                    case "time":{
                        System.out.println("Your logical time is: " + netNode.getLogicalTime());
                        break;
                    }
                    case "help":{
                        System.out.println("----HELP----");
                        System.out.println("send <ip:port> <amount>  -  ip:port of the recipient, AMOUNT to be transfered");
                        System.out.println("balance  -  get balance of your account");
                        System.out.println("leader  -  get ip:port of current leader");
                        System.out.println("table  -  get LOCAL table of accounts and balances");
                        System.out.println("tversion  -  get version of LOCAL table of accounts and balances");
                        System.out.println("next  -  get your NEXT node");
                        System.out.println("prev  -  get your PREV node");
                        System.out.println("!quit  -  shutdown current process");
                        System.out.println("---END of HELP---");
                        break;
                    }
                    case "!quit":{
//                      LOG.debug("#main: exiting");
                        netNode.onClose();
                        System.exit(0);
                        break;
                    }
                    default:{
                        System.out.println("UNKNOWN COMMAND - type help for command list");
                    }
                }

            } catch (IOException e) {
//                LOG.error("#main: ", e);
            } catch (NotBoundException e) {
                e.printStackTrace();
            }
        }
    }
}
