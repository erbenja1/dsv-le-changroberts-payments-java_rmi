package cz.cvut.dsv.DTO;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Message that should be sent in natural direction and inform all nodes about new version of balance table
 */
public class MessageTableUpdate implements Serializable {
    private HashMap<String, Integer> balanceTable;
    private final Integer tableVersion;
    private long logicalTime;

    public MessageTableUpdate(HashMap<String, Integer> balanceTable, Integer tableVersion, long logicalTime) {
        this.balanceTable = balanceTable;
        this.tableVersion = tableVersion;
        this.logicalTime = logicalTime;
    }

    public HashMap<String, Integer> getBalanceTable() {
        return balanceTable;
    }

    public Integer getTableVersion() {
        return tableVersion;
    }

    public long getLogicalTime() {
        return logicalTime;
    }

    public void setLogicalTime(long logicalTime) {
        this.logicalTime = logicalTime;
    }

    @Override
    public String toString() {
        return "MessageTableUpdate [version =" + tableVersion + "]";
    }
}
