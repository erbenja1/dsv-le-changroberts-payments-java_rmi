package cz.cvut.dsv.rmi_com;

import cz.cvut.dsv.DTO.MessageBalanceUpdate;
import cz.cvut.dsv.DTO.MessageTableUpdate;
import cz.cvut.dsv.DTO.Payment;
import cz.cvut.dsv.DTO.SingleNode;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PaymentOperation extends Remote {
    public void transfer(Payment pmt) throws RemoteException;
    public Integer getAccountBalance(SingleNode node, long lTime) throws RemoteException;
    public void accountBalanceUpdate(MessageBalanceUpdate msg) throws RemoteException;
    public void updateBalanceTable(MessageTableUpdate msg) throws RemoteException;
}
