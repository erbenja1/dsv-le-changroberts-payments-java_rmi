package cz.cvut.dsv.DTO;

import java.io.Serializable;

public class Node implements Serializable {
    private SingleNode me;
    private SingleNode prevNode;
    private SingleNode nextNode;

    public Node(){
        super();
    }

    public Node(String address){
        me = new SingleNode(address);
    }

    public SingleNode getMe() {
        return me;
    }

    public void setMe(SingleNode me) {
        this.me = me;
    }

    public SingleNode getPrevNode() {
        return prevNode;
    }

    public void setPrevNode(SingleNode prevNode) {
        this.prevNode = prevNode;
    }

    public SingleNode getNextNode() {
        return nextNode;
    }

    public void setNextNode(SingleNode nextNode) {
        this.nextNode = nextNode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Node other = (Node) obj;
        if (nextNode == null) {
            if (other.nextNode != null)
                return false;
        } else if (!nextNode.equals(other.nextNode))
            return false;
        if (me == null) {
            if (other.me != null)
                return false;
        } else if (!me.equals(other.me))
            return false;
        if (prevNode == null) {
            if (other.prevNode != null)
                return false;
        } else if (!prevNode.equals(other.prevNode))
            return false;
        return true;
    }
}
